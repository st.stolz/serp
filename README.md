# sErp

A simple bill generator (LaTeX PDF), scripted in PHP with simple docker
installation. An educational example for cutting edge Database education and docker usage
with GitLab CI / CD.

sErp uses plane PHP and JavaScript without Framework to show:

* [AJAX Requests](https://gitlab.com/st.stolz/serp/blob/0.9/serp/inc_body_header.php#L14)
* [PHP SESSION](https://gitlab.com/st.stolz/serp/blob/0.9/serp/inc_header.php#L14)
* [FILE Operations](https://gitlab.com/st.stolz/serp/blob/cb454c26e5091aadb5021459626422ba1ae6aa1a/serp/latex_bill_export.php#L47)
* [Database Operations with PDO](https://gitlab.com/st.stolz/serp/blob/cb454c26e5091aadb5021459626422ba1ae6aa1a/serp/orders_view.php#L42)

SQL to show:

* [Data Definition Language for automatic database setup](https://gitlab.com/st.stolz/serp/blob/cb454c26e5091aadb5021459626422ba1ae6aa1a/serp/database.sql)
* [JOINS, GROUP BY to retrieve data](https://gitlab.com/st.stolz/serp/blob/cb454c26e5091aadb5021459626422ba1ae6aa1a/serp/orders_view.php#L33)
* [LIMIT and Offset for pagination](https://gitlab.com/st.stolz/serp/blob/cb454c26e5091aadb5021459626422ba1ae6aa1a/serp/orders_view.php#L46)
* [aggregate functions for statistical data](https://gitlab.com/st.stolz/serp/blob/cb454c26e5091aadb5021459626422ba1ae6aa1a/serp/orders_view.php#L36)

Docker to show

* [homogenisation of development environment](https://gitlab.com/st.stolz/serp/blob/cb454c26e5091aadb5021459626422ba1ae6aa1a/docker-compose-dev-full.yml)
* [easy setup of complex program with multiple servers and complex programs (MYSQL,
  Apache, TeX Live)](https://gitlab.com/st.stolz/serp/blob/cb454c26e5091aadb5021459626422ba1ae6aa1a/Dockerfile.full)
* [GitLab CI/CD for automatic building the containers](https://gitlab.com/st.stolz/serp/blob/cb454c26e5091aadb5021459626422ba1ae6aa1a/.gitlab-ci.yml)
* [docker-compose for combination of different containers and setups](https://gitlab.com/st.stolz/serp/blob/cb454c26e5091aadb5021459626422ba1ae6aa1a/docker-compose-full.yml)

LaTeX to show

* easy templating for professional documents

## Installation and Setup

* `docker-compose-full.yml` has texlive installed and needs about 3.3 GB of disk space.
* `docker-compose.yml` comes without texlive (no printing of bills to pdf) and needs about 1 GB of disk space.

1. Download the docker-compose-full.yml or docker-compose.yml to your hard drive
2. Go to the folder where you have downloaded the file and type in following
   commands in docker command line windows

```bash
# only needed the first time for not public projects. You need your GitLab credentials
# This project is public to this time, so not needed
# docker login registry.gitlab.com
# replace "docker-compose.yml" by "docker-compose-full.yml" if you need pdf export
docker-compose -f docker-compose.yml up -d
```