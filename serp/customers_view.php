<?php include("inc_header.php"); ?>
<?php

if(isset($_GET['add']) && $_GET['add'] == true){
	$add_customer = true;
}
else {
	$add_customer = false;
}
if(isset($_GET['offset'])){
	$offset = $_GET['offset'];
}
else{
	$offset = 0;
}
$limit = 10;

if(isset($_POST['add_customer']) && isset($_POST['form_hash']) && isset($_POST['customer_name'])){
	$message[] = "Inserting customer with name '".$_POST['customer_name']."'";
	$sql_insert_product = 'INSERT INTO customers (name,street,town,contact_name) 
				VALUES (:customer_name,:customer_street,:customer_town,:contact_name)';
	$stmt = $conn->prepare($sql_insert_product);
	$stmt -> bindParam(':customer_name', $_POST['customer_name']);
	$stmt -> bindParam(':customer_street', $_POST['customer_street']);
	$stmt -> bindParam(':customer_town', $_POST['customer_town']);
	$stmt -> bindParam(':contact_name', $_POST['customer_contact']);
	$stmt -> execute();
}

$sql = 'SELECT customers.id as "customer_id",
					customers.name as "customers_name", 
					count(orders.id) as "nOrders" 
				FROM customers 
					LEFT JOIN orders ON customers.id = orders.customers_id 
				GROUP BY customers.id';
$stmt = $conn->prepare($sql);
$stmt->execute();
$number_all = $stmt->rowCount();
$sql_limit = $sql.'
			LIMIT :offset,:limit
		';
$stmt = $conn->prepare($sql_limit);
$stmt -> bindParam(':offset', $offset, PDO::PARAM_INT);
$stmt -> bindParam(':limit', $limit, PDO::PARAM_INT);
$stmt->execute();
?>

<?php include("inc_body_header.php"); ?>

<h1>Kunden</h1>

<?php if($add_customer && !isset($_POST['add_customer'])): ?>
	<?php include("add_customer.php"); ?>
<?php endif ?>

<div class="divider"></div>

<div class="divider"></div>
<div class="section">
	<table class="striped">
		<tr>
			<th>Kunde</th>
			<th>Bestellungen</th>
			<th></th>
		</tr>

	<?php foreach($stmt as $row): ?>
		<tr>
			<td><?= $row['customers_name'] ?></td>
			<td><a href="orders_view.php?customer_id=<?= $row['customer_id'] ?>"><?= $row['nOrders'] ?></a></td>	
			<?php $queryString = basename($_SERVER['PHP_SELF']).'?'.http_build_query(array("delete"=>"customers","id"=>$row['customer_id'],"hash"=>$form_hash)); ?>
			<td>
			<?php if($row['nOrders'] == 0): ?>
				<a class="waves-effect waves-teal btn-flat" href="<?=$queryString?>"><i class="material-icons">delete_forever</i></a>
			<?php endif ?>
			</td>
		</tr>
	<?php endforeach ?>

	</table>
</div>

<ul class="pagination">
	<?php $backOffset = ($offset>$limit)?$offset-$limit:0;?>
	<?php $queryString = basename($_SERVER['PHP_SELF']).'?'.http_build_query(array("offset"=>$backOffset)); ?>
    <li class="<?=($offset>=$limit)?"waves-effect":"disabled"?>">
		
		<a href="<?=$queryString?>">
		
			<i class="material-icons">chevron_left</i>
		
		</a>
		
	</li>
    <?php for($i = 0; $i < $number_all / $limit; $i++): ?>
	<?php $queryString = basename($_SERVER['PHP_SELF']).'?'.http_build_query(array("offset"=>$i*$limit)); ?>
	<li class="<?=($i*$limit == $offset)?"active blue-grey":"waves-effect"?>">
		<a href="<?=$queryString?>"><?=$i+1?></a>
	</li>
    <?php endfor ?>
	<?php $nextOffset = ($number_all>$offset+$limit)?$offset+$limit:$offset;?>
	<?php $queryString = basename($_SERVER['PHP_SELF']).'?'.http_build_query(array("offset"=>$nextOffset)); ?>
    <li class="<?=($offset+$limit<$number_all)?"waves-effect":"disabled"?>">
		<a href="<?=$queryString?>"><i class="material-icons">chevron_right</i></a>
	</li>
</ul>

<div class="fixed-action-btn">
	<?php $queryString = basename($_SERVER['PHP_SELF']).'?'.http_build_query(array("add"=>"true")); ?>
	<a class="btn-floating btn-large red" href="<?=$queryString?>">
		<i class="large material-icons">add</i>
	</a>  
</div>

<?php include("inc_footer.php"); ?>