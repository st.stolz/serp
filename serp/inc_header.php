<?php
session_start();
$form_hash = md5(time()+rand());
?>
<?php
$host = "serp-mysql";
$user = "root";
$pass = "123";
$database = "serp";
$fileOutputName = "downloads/output_ready.pdf";
$message = array();
$js = array();

/**
 * Downloads Session Management
 */
// be sure that Session Array exists which holds download info
if(!isset($_SESSION['waitForDownload']) || !is_array($_SESSION['waitForDownload'])){
  $_SESSION['waitForDownload'] = array();
}
// if there are downloads pending, check to delete already downloaded
if(count($_SESSION['waitForDownload'])>0){
  foreach($_SESSION['waitForDownload'] as $key => $value){
    if($value == 'downloaded'){      
      unlink('downloads/'.$key.'.pdf') or die("Couldn't delete file");
      unset($_SESSION['waitForDownload'][$key]);    
    }    
  }
}

/**
 * Database Server connection
 */
// Testing in a loop if database is up - mysql docker container needs a little
// bit longer
$running = true;
while($running){
  try {
    $conn = new PDO('mysql:host='.$host.';charset=utf8', $user, $pass);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $running = false;
  }
  catch(PDOException $e) {
    //echo "Connection to $host failed: " . $e->getMessage();
    sleep(1);
  }

}

/**
 * Select Database
 */
try {
  $conn->exec('USE '.$database);
}
catch(PDOException $e) {
  // if not possible to select, create Database
	$message[] = 'Database "'.$database.'" does not exist. Creating database ...';
  include("create_database.php");
  try {
    $conn->exec('USE '.$database);
  }
  catch(PDOException $e2) {
    echo "Setting up database failed: " . $e2->getMessage();
  }
}

/**
 * Delete Articles, Customers, Orders or Order_Details
 */
if(isset($_GET['delete'])&&isset($_GET['id'])&&isset($_GET['delete'])){
  $message[] = "Delete id ".$_GET['id']." from table '".$_GET['delete']."'";
  // prevent SQL injection by parsing value
  switch($_GET['delete']){
    case "articles":
      $tbl = "articles";
      break;
    case "customers":
      $tbl = "customers";
      break;
    case "orders":
      $tbl = "orders";
      break;
    case "order_details":
      $tbl = "order_details";
      break;
  }
	$sql_delete_row = 'DELETE FROM '.$tbl.' WHERE id = :id';
	$stmt = $conn->prepare($sql_delete_row);
	$stmt -> bindParam(':id', $_GET['id']);
	$stmt -> execute();
}

/**
 * Reset Database
 */
if(isset($_GET['reset']) && $_GET['reset']=='true'){
  $message[] = 'Resetting Database';
  include("create_database.php");
}
?>


