<?php include("inc_header.php"); ?>

<?php 
$customerid = "%";
if(isset($_GET['customer_id'])){
	$customerid = $_GET['customer_id'];
}

if(isset($_GET['add']) && $_GET['add'] == true){
	$add_order = true;
}
else {
	$add_order = false;
}
if(isset($_GET['offset'])){
	$offset = $_GET['offset'];
}
else{
	$offset = 0;
}
$limit = 10;

$baseQueryArray = array("customer_id"=>$customerid);

if(isset($_POST['add_order']) && isset($_POST['form_hash']) && isset($_POST['customer_id'])){
	$message[] = "Inserting order for customer '".$_POST['customer_id']."'";
	$sql_insert_product = 'INSERT INTO orders (customers_id) VALUES (:customer_id)';
	$stmt = $conn->prepare($sql_insert_product);
	$stmt -> bindParam(':customer_id', $_POST['customer_id']);
	$stmt -> execute();
}

$sql = "SELECT orders.id as 'orders_id', 
					customers.name as 'customers_name', 
					orders.order_date as 'orders_date',
					count(order_details.id) as 'numberPositions'
				FROM orders 
					LEFT JOIN customers ON customers.id = orders.customers_id
					LEFT JOIN order_details ON order_details.orders_id = orders.id
				WHERE customers.id LIKE '".$customerid."'
				GROUP BY orders.id";
$stmt = $conn->prepare($sql);
$stmt->execute();
$number_all = $stmt->rowCount();
$sql_limit = $sql.'
			LIMIT :offset,:limit
		';
$stmt = $conn->prepare($sql_limit);
$stmt -> bindParam(':offset', $offset, PDO::PARAM_INT);
$stmt -> bindParam(':limit', $limit, PDO::PARAM_INT);
$stmt->execute();
?>

<?php include("inc_body_header.php"); ?>

<h1>Bestellungen</h1>

<?php if($add_order && !isset($_POST['add_order'])): ?>
	<?php include("add_order.php"); ?>
<?php endif ?>

<div class="divider"></div>

<div class="section">

<p><?=$number_all?> Bestellungen gesamt</p>

<table class="striped">
	<tr>
		<th>BestNr</th>
		<th>Bestelldatum</th>
		<th>Besteller</th>
		<th>Positionen</th>
		<th>Rechnung</th>
		<th></th>
	</tr>
<?php foreach($stmt as $row): ?>
	<?php
		$orderNr = $row['orders_id'];
		$orderDate = $row['orders_date'];
		$customerName = $row['customers_name'];
		$numberPositions = $row['numberPositions'];
		$orderNrHash = substr(md5($orderNr.$orderDate.$customerName.$numberPositions),0,8);		
	?>		
	<tr>
		<td><?=$orderNr?> (<?=$orderNrHash?>)</td>
		<td><?=$orderDate?></td>
		<td><?=$customerName?></td>
		<td><?=$numberPositions?></td>
		<td><a href="order_view.php?order_id=<?=$row['orders_id']?>" class="waves-effect waves-teal btn-flat"><i class="material-icons right">send</i></a></td>
		<?php $queryString = basename($_SERVER['PHP_SELF']).'?'.http_build_query(array("delete"=>"orders","id"=>$orderNr,"hash"=>$form_hash)); ?>
		<td>
		<?php if($row['numberPositions'] == 0): ?>
			<a class="waves-effect waves-teal btn-flat" href="<?=$queryString?>"><i class="material-icons">delete_forever</i></a>
		<?php endif ?>
		</td>
	</tr>
<?php endforeach ?>

</table>
</div>

<ul class="pagination">
	<?php $backOffset = ($offset>$limit)?$offset-$limit:0;?>
	<?php $queryString = basename($_SERVER['PHP_SELF']).'?'.http_build_query(array_merge($baseQueryArray,array("offset"=>$backOffset))); ?>
    <li class="<?=($offset>=$limit)?"waves-effect":"disabled"?>">
		
		<a href="<?=$queryString?>">
		
			<i class="material-icons">chevron_left</i>
		
		</a>
		
	</li>
    <?php for($i = 0; $i < $number_all / $limit; $i++): ?>
	<?php $queryString = basename($_SERVER['PHP_SELF']).'?'.http_build_query(array_merge($baseQueryArray,array("offset"=>$i*$limit))); ?>
	<li class="<?=($i*$limit == $offset)?"active blue-grey":"waves-effect"?>">
		<a href="<?=$queryString?>"><?=$i+1?></a>
	</li>
    <?php endfor ?>
	<?php $nextOffset = ($number_all>$offset+$limit)?$offset+$limit:$offset;?>
	<?php $queryString = basename($_SERVER['PHP_SELF']).'?'.http_build_query(array_merge($baseQueryArray,array("offset"=>$nextOffset))); ?>
    <li class="<?=($offset+$limit<$number_all)?"waves-effect":"disabled"?>">
		<a href="<?=$queryString?>"><i class="material-icons">chevron_right</i></a>
	</li>
</ul>

<div class="fixed-action-btn">
	<?php $queryString = basename($_SERVER['PHP_SELF']).'?'.http_build_query(array_merge($baseQueryArray,array("add"=>"true"))); ?>
	<a class="btn-floating btn-large red" href="<?=$queryString?>">
		<i class="large material-icons">add</i>
	</a>  
</div>

<?php include("inc_footer.php"); ?>