<?php
$sql_products = "SELECT 
        customers.id as 'customer_id', 
        customers.name as 'customer_name'
    FROM customers";
$stmt = $conn->prepare($sql_products);
$stmt -> execute();
$result_customers = $stmt->fetchAll();
?>
<div class="row">
    <form class="col s12" method="post" action="<?=basename($_SERVER['REQUEST_URI'])?>">
        <input type="hidden" name="add_order" value="true"> 
        <input type="hidden" name="form_hash" value="<?=$form_hash?>"> 
        <div class="row">
            <div class="input-field col s4">
                <select name="customer_id">
                    <option value="" disabled selected>Choose customer</option>
                    <?php foreach($result_customers as $customer): ?>                    
                    <option value="<?=$customer['customer_id']?>"><?=$customer['customer_name']?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <div class="input-field col s4">
                <button class="btn waves-effect waves-light" type="submit" name="action">Add new
                    <i class="material-icons right">send</i>
                </button>
            </div>
        </div>
      
    </form>
</div>