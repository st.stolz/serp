<div class="row">
    <form class="col s12" method="post" action="<?=basename($_SERVER['REQUEST_URI'])?>">
        <input type="hidden" name="add_customer" value="true"> 
        <input type="hidden" name="form_hash" value="<?=$form_hash?>"> 
        <div class="row">
            <div class="input-field col s3">
                <input id="customer_name" name="customer_name" type="text" class="validate">
                <label for="customer_name">Name</label>
            </div>

            <div class="input-field col s3">
                <input id="customer_street" name="customer_street" type="text" class="validate">
                <label for="customer_street">Street</label>
            </div>

            <div class="input-field col s3">
                <input id="customer_town" name="customer_town" type="text" class="validate">
                <label for="customer_town">Town</label>
            </div>

            <div class="input-field col s3">
                <input id="customer_contact" name="customer_contact" type="text" class="validate">
                <label for="customer_contact">Contact Person</label>
            </div>
        </div>  
        <div class="row">
            <div class="input-field col s3">
                <button class="btn waves-effect waves-light" type="submit" name="action">Add new
                    <i class="material-icons right">send</i>
                </button>
            </div>
        </div>      
    </form>
</div>