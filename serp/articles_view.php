<?php include("inc_header.php"); ?>
<?php

if(isset($_GET['add']) && $_GET['add'] == true){
	$add_article = true;
}
else {
	$add_article = false;
}
if(isset($_GET['offset'])){
	$offset = $_GET['offset'];
}
else{
	$offset = 0;
}
$limit = 10;

if(isset($_POST['add_article']) && isset($_POST['form_hash']) && isset($_POST['article_name']) && isset($_POST['price'])){
	$message[] = "Inserting article with name '".$_POST['article_name']."'";
	$sql_insert_product = 'INSERT INTO articles (name, price) VALUES (:article_name, :price)';
	$stmt = $conn->prepare($sql_insert_product);
	$stmt -> bindParam(':article_name', $_POST['article_name']);
	$stmt -> bindParam(':price', $_POST['price']);
	$stmt -> execute();
}

$sql = 'SELECT 	articles.id as "article_id",
				articles.name as "article_name",
				articles.price as "article_price",
				COALESCE(sum(order_details.amount),0) as "ordered_amount"
			FROM articles
			LEFT JOIN order_details ON order_details.articles_id = articles.id
			GROUP BY articles.id
		';
$stmt = $conn->prepare($sql);
$stmt->execute();
$number_all = $stmt->rowCount();
$sql_limit = $sql.'
			LIMIT :offset,:limit
		';
$stmt = $conn->prepare($sql_limit);
$stmt -> bindParam(':offset', $offset, PDO::PARAM_INT);
$stmt -> bindParam(':limit', $limit, PDO::PARAM_INT);
$stmt->execute();
//$result = $stmt->fetchAll();

?>

<?php include("inc_body_header.php"); ?>

<h1>Artikel</h1>

<?php if($add_article && !isset($_POST['add_article'])): ?>
	<?php include("add_article.php"); ?>
<?php endif ?>

<div class="divider"></div>

<div class="section">
	<table class="striped">
		<tr>
			<th>Artikel</th>
			<th><i class="material-icons prefix">euro_symbol</i></th>
			<th>Bestellt</th>
			<th></th>
		</tr>

	<?php foreach($stmt as $row): ?>
		
		<tr>
			<td><?= $row['article_name'] ?></td>
			<td><?= $row['article_price'] ?> €</td>	
			<td><?= $row['ordered_amount'] ?></td>	
			<?php $queryString = basename($_SERVER['PHP_SELF']).'?'.http_build_query(array("delete"=>"articles","id"=>$row['article_id'],"hash"=>$form_hash)); ?>
			<td>
			<?php if($row['ordered_amount'] == 0): ?>
				<a class="waves-effect waves-teal btn-flat" href="<?=$queryString?>"><i class="material-icons">delete_forever</i></a>
			<?php endif ?>
			</td>
		</tr>
	<?php endforeach ?>

	</table>
</div>

<ul class="pagination">
	<?php $backOffset = ($offset>$limit)?$offset-$limit:0;?>
	<?php $queryString = basename($_SERVER['PHP_SELF']).'?'.http_build_query(array("offset"=>$backOffset)); ?>
    <li class="<?=($offset>=$limit)?"waves-effect":"disabled"?>">
		
		<a href="<?=$queryString?>">
		
			<i class="material-icons">chevron_left</i>
		
		</a>
		
	</li>
    <?php for($i = 0; $i < $number_all / $limit; $i++): ?>
	<?php $queryString = basename($_SERVER['PHP_SELF']).'?'.http_build_query(array("offset"=>$i*$limit)); ?>
	<li class="<?=($i*$limit == $offset)?"active blue-grey":"waves-effect"?>">
		<a href="<?=$queryString?>"><?=$i+1?></a>
	</li>
    <?php endfor ?>
	<?php $nextOffset = ($number_all>$offset+$limit)?$offset+$limit:$offset;?>
	<?php $queryString = basename($_SERVER['PHP_SELF']).'?'.http_build_query(array("offset"=>$nextOffset)); ?>
    <li class="<?=($offset+$limit<$number_all)?"waves-effect":"disabled"?>">
		<a href="<?=$queryString?>"><i class="material-icons">chevron_right</i></a>
	</li>
</ul>

<div class="fixed-action-btn">
	<?php $queryString = basename($_SERVER['PHP_SELF']).'?'.http_build_query(array("add"=>"true")); ?>
	<a class="btn-floating btn-large red" href="<?=$queryString?>">
		<i class="large material-icons">add</i>
	</a>  
</div>

<?php include("inc_footer.php"); ?>