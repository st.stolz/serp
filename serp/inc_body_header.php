<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <title>Offer</title>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
  
  </head>
  <script>
    function sleep(ms) {
      return new Promise(resolve => setTimeout(resolve, ms));
    }
    async function checkForDownload() {
        $counter = 0;
        while (true) {
            await sleep(1000);
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    if(this.responseText == 'true'){
                        var badge = document.getElementById("new_download_badge");
                        badge.style.display = "inline";
                        badge = document.getElementById("new_download_badge_sidebar");
                        badge.style.display = "inline";
                    }
                }
            };
            xhttp.open("GET", "isNewDownload.php", true);
            xhttp.send();
            $counter++;
        }
        
    }
    </script>
    
<body>   

<nav class="blue-grey">
  <div class="nav-wrapper container">
    <a href="./" class="brand-logo"><img src="Logo.png" alt="Logo" height="60px"></a>
    <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    <ul id="nav-mobile" class="right hide-on-med-and-down">
      <li><a href="articles_view.php">Articles</a></li>
      <li><a href="customers_view.php">Customers</a></li>
      <li><a href="orders_view.php">Orders</a></li>
      <li><a href="downloads_view.php">Downloads <span id="new_download_badge" class="new badge red" style="display: none;"></span></a></li>
      <li><a href="index.php?reset=true">Reset Db</a></li>
    </ul>
  </div>
</nav>
<ul class="sidenav" id="mobile-demo">
    <li><a href="articles_view.php">Articles</a></li>
    <li><a href="customers_view.php">Customers</a></li>
    <li><a href="orders_view.php">Orders</a></li>
    <li><a href="downloads_view.php">Downloads <span id="new_download_badge_sidebar" class="new badge red" style="display: none;"></span></a></li>
    <li><a href="index.php?reset=true">Reset Database</a></li>
</ul>

<div class="container">

<?php if(sizeof($message) > 0): ?>

<div class="row">
    <div class="col s12">
      <div class="card blue-grey lighten-4">
        <div class="card-content black-text">
          <span class="card-title">Message</span>
          <?php foreach($message as $msg): ?>
              <p><?php echo $msg;?></p>
          <?php endforeach; ?>
        </div>        
      </div>
    </div>
  </div>

<?php endif; ?>

