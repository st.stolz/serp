<?php include("inc_header.php"); ?>
<?php
if(isset($_GET['order_id']))
{
	$order_id = $_GET['order_id'];
}
else {
	$order_id = 0;
}
if(isset($_GET['add']) && $_GET['add'] == true){
	$add_product = true;
}
else {
	$add_product = false;
}
if(isset($_GET['offset'])){
	$offset = $_GET['offset'];
}
else{
	$offset = 0;
}
$limit = 10;

$baseQueryArray = array("order_id"=>$order_id);

$sql = "	SELECT 
				orders.id as 'order_id', 
				customers.name as 'customers_name', 
				customers.street as 'customers_street', 
				customers.town as 'customers_town', 
				customers.contact_name as 'contact_name', 
				orders.order_date as 'order_date' 
			FROM orders 
				LEFT JOIN customers ON customers.id = orders.customers_id 
			WHERE orders.id = ".$order_id.";";

$query = $conn->query($sql);
$result = $query->fetch();

$orderNr = $result['order_id'];
$reNr = $result['order_id'];
$custName = $result['customers_name'];
$custStreet = $result['customers_street'];
$custTown = $result['customers_town'];
$contactName = $result['contact_name'];
$orderDate = date_format(date_create($result['order_date']), 'd.m.Y');
$reNrHash = substr(md5($reNr.$custName.$orderDate),0,8);

if(isset($_POST['add_product']) && isset($_POST['form_hash']) && isset($_POST['article_id']) && isset($_POST['amount'])){
	$message[] = "Inserting product '".$_POST['article_id']."'";
	$sql_insert_product = 'INSERT INTO order_details (amount, articles_id, orders_id) VALUES (:amount,:articles_id,:orders_id)';
	$stmt = $conn->prepare($sql_insert_product);
	$stmt -> bindParam(':amount', $_POST['amount']);
	$stmt -> bindParam(':articles_id', $_POST['article_id']);
	$stmt -> bindParam(':orders_id', $orderNr);
	$stmt -> execute();
}


/**
 * SQL Statement to get positions data
 */
$sql = 
	'SELECT 		
		order_details.id as "order_details_id", 
		order_details.amount as "product_amount", 
		order_details.articles_id as "product_id",
		articles.name as "articleName",
		articles.price as "articlePrice",
		articles.price * order_details.amount as "preisPosition"
	FROM orders 
		JOIN customers ON customers.id = orders.customers_id 
		JOIN order_details ON order_details.orders_id = orders.id 
		JOIN articles ON order_details.articles_id = articles.id 
	WHERE orders.id = :id';
$stmt = $conn->prepare($sql);
$stmt->bindParam(':id', $order_id);
$stmt->execute();
$number_all = $stmt->rowCount();
$sql_limit = $sql.'
			LIMIT :offset,:limit
		';
$stmt = $conn->prepare($sql_limit);
$stmt->bindParam(':id', $order_id);
$stmt->bindParam(':offset', $offset, PDO::PARAM_INT);
$stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
$stmt->execute();

$orderNrHash = substr(md5($orderNr.$orderDate.$custName.$number_all),0,8);

/**
 * print to LaTeX if requested
 */
if(isset($_POST['print']) && $_POST['print'] == "true"){

	include("latex_bill_export.php");

}

/**
 * Show data online in html
 */
?>

<?php include("inc_body_header.php"); ?>

<h1>Bestellung <?= $reNr ?> (<?= $orderNrHash ?>)</h1>

<?php if($add_product && !isset($_POST['add_product'])): ?>
	<?php include("add_order_detail.php"); ?>
<?php endif ?>

<div class="divider"></div>

<div class="row">
	<div class="col s12 m6">
		<div class="card green lighten-4">
			<div class="card-content black-text">
					<span class="card-title"><?php echo $custName;?><br></span>
					<table class="">	
						<tbody>
						<tr>
							<td>Auftragsdatum</td>
							<td><?php echo $orderDate;?></td>
						</tr>
						<tr>
							<td>Rechnungsnummer</td>
							<td><?php echo $reNr.' ('.$reNrHash.')';?></td>
						</tr>				
						</tbody>
					</table>
			</div>
			<div class="card-action green-text">
			<?php $queryString = basename($_SERVER['PHP_SELF']).'?'.http_build_query(array("order_id"=>$order_id)); ?>
				<form id="form" method="post" action="<?=$queryString?>">
					<input type="hidden" name="print" value="true"> 
					<a onclick="document.getElementById('form').submit(); return false;" class="green-text btn-flat"><i class="material-icons">print</i></a>
				</form>
			</div>
		</div>
	</div>
</div>
	
<p><?= $number_all ?> Postionen</p>

<table>
	<tr><th>Menge</th>
			<th>Bezeichnung</th>
			<th style="text-align: right">Preis</th>
			<th style="text-align: right">Gesamtpreis</th>
			<th></th>
	</tr>
	<?php	
		$sumPrice = 0;
		foreach($stmt as $row): 
		$productAmount = $row['product_amount'];
		$pricePerPos = $row['preisPosition'];
		$sumPrice += $pricePerPos; ?>

	<tr>
		<td><?=$productAmount?></td>
		<td><?=$row['articleName']?></td>
		<td style="text-align: right"><?=$row['articlePrice']?> €</td>
		<td style="text-align: right"><?=$pricePerPos?> €</td>
		<?php $queryString = basename($_SERVER['PHP_SELF']).'?'.http_build_query(array("order_id"=>$order_id,"delete"=>"order_details","id"=>$row['order_details_id'],"hash"=>$form_hash)); ?>
		<td>
			<a class="waves-effect waves-teal btn-flat" href="<?=$queryString?>"><i class="material-icons">delete_forever</i></a>
		</td>
	</tr>
	<?php endforeach ?>
</table>

<p style='text-align: right'>Gesamtpreis Bestellung: <?=$sumPrice?> €</p>

<ul class="pagination">
	<?php $backOffset = ($offset>$limit)?$offset-$limit:0;?>
	<?php $queryString = basename($_SERVER['PHP_SELF']).'?'.http_build_query(array_merge($baseQueryArray,array("offset"=>$backOffset))); ?>
    <li class="<?=($offset>=$limit)?"waves-effect":"disabled"?>">
		
		<a href="<?=$queryString?>">
		
			<i class="material-icons">chevron_left</i>
		
		</a>
		
	</li>
    <?php for($i = 0; $i < $number_all / $limit; $i++): ?>
	<?php $queryString = basename($_SERVER['PHP_SELF']).'?'.http_build_query(array_merge($baseQueryArray,array("offset"=>$i*$limit))); ?>
	<li class="<?=($i*$limit == $offset)?"active blue-grey":"waves-effect"?>">
		<a href="<?=$queryString?>"><?=$i+1?></a>
	</li>
    <?php endfor ?>
	<?php $nextOffset = ($number_all>$offset+$limit)?$offset+$limit:$offset;?>
	<?php $queryString = basename($_SERVER['PHP_SELF']).'?'.http_build_query(array_merge($baseQueryArray,array("offset"=>$nextOffset))); ?>
    <li class="<?=($offset+$limit<$number_all)?"waves-effect":"disabled"?>">
		<a href="<?=$queryString?>"><i class="material-icons">chevron_right</i></a>
	</li>
</ul>

<div class="fixed-action-btn">
	<?php $queryString = basename($_SERVER['PHP_SELF']).'?'.http_build_query(array_merge($baseQueryArray,array("add"=>"true","order_id"=>$order_id))); ?>
	<a class="btn-floating btn-large red" href="<?=$queryString?>">
		<i class="large material-icons">add</i>
	</a>  
</div>

<?php include("inc_footer.php"); ?>