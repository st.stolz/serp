<?php include("inc_header.php"); ?>
<?php include("inc_body_header.php"); ?>

<?php
$downloadFiles = array();
if(is_dir("downloads")){
    $handle = opendir("downloads");
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
            $downloadFiles[] = $entry;
            $_SESSION['waitForDownload'][pathinfo($entry)['filename']] = 'viewed';
        }
    }
    closedir($handle);
}

?>

<table class="striped">
    <thead>
        <tr>
            <th>Download</th>
            <th></th>
            <th></th>
        </tr>
    </thead>

    <tbody>   
        <?php foreach($downloadFiles as $filename): ?> 
        <tr>
            <td><?=$filename?></td>
            <?php $queryStringBaseArr = array("file"=>$filename,"hash"=>$form_hash); ?>
            <?php $queryString = basename($_SERVER['PHP_SELF']).'?'.http_build_query(array_merge($queryStringBaseArr,array("action"=>"delete"))); ?>
            <td><a href="<?=$queryString?>"><i class="material-icons">delete_forever</i></a></td>
            <?php $queryString = 'download_output.php?'.http_build_query(array_merge($queryStringBaseArr,array("action"=>"download"))); ?>
            <td><a href="<?=$queryString?>"><i class="material-icons">file_download</i></td>
        </tr>    
        <?php endforeach ?> 
    </tbody>
</table>

<?php include("inc_footer.php"); ?>