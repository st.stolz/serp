<?php
session_start();
if(isset($_GET['file']) && $_GET['action']=='download'){
    $file_url = 'downloads/'.$_GET['file'];
    header('Content-Type: application/pdf');
    header("Content-Transfer-Encoding: Binary"); 
    header("Content-disposition: attachment; filename=\"" . basename($file_url) . "\""); 
    readfile($file_url); // do the double-download-dance (dirty but worky)
    $_SESSION['waitForDownload'][pathinfo($_GET['file'])['filename']] = 'downloaded';
    exec('rm pdflatex*.fls > /dev/null &');
}
exit(); 
?>