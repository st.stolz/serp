<?php
$sql_products = "SELECT 
        articles.id as 'article_id', 
        articles.name as 'article_name', 
        articles.price as 'article_price'
    FROM articles";
$stmt = $conn->prepare($sql_products);
$stmt -> execute();
$result_articles = $stmt->fetchAll();
?>

<div class="row">
    <form class="col s12" method="post" action="<?=basename($_SERVER['REQUEST_URI'])?>">
        <input type="hidden" name="add_product" value="true"> 
        <input type="hidden" name="form_hash" value="<?=$form_hash?>"> 
        <div class="row">
            <div class="input-field col s4">
                <input id="amount" name="amount" type="text" class="validate">
                <label for="amount">Amount</label>
            </div>

            <div class="input-field col s4">
                <select name="article_id">
                    <option value="" disabled selected>Choose article</option>
                    <?php foreach($result_articles as $article): ?>                    
                    <option value="<?=$article['article_id']?>"><?=$article['article_name']?></option>
                    <?php endforeach ?>
                </select>
            </div>
            
            <div class="input-field col s4">
                <button class="btn waves-effect waves-light" type="submit" name="action">Add new
                    <i class="material-icons right">send</i>
                </button>
            </div>
        </div>
      
    </form>
</div>