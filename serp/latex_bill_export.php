<?php

    $message[] = "Printing current bill to LaTeX. You will receive a message when it is ready for printing";

	$stmtLatex = $conn->prepare($sql);
	$stmtLatex->bindParam(':id', $order_id);
	$stmtLatex->execute();

	$str = "\begin{center}\n";
	$str .= "\begin{tabular}{l l r r r }\n";
	//$str .= "\hline\n";
	$str .= '\textbf{Pos} & \textbf{Produkt} & \textbf{Menge} & \textbf{Preis} & \textbf{\unit[Betrag]{\euro}}'.'\\\\'."\n";
	$str .= "\hline\n";

	$sumPrice = 0;
	setlocale(LC_MONETARY, 'de_DE');
	$counter = 0;
	foreach($stmtLatex as $row){
		$counter++;
		$productAmount = $row['product_amount'];
		$articlePrice = $row["articlePrice"];
		$articlePriceStr = number_format($articlePrice,2,',','.');
		$pricePerPos = $row['preisPosition'];
		$pricePerPosStr = number_format($pricePerPos,2,',','.');
		$sumPrice += $pricePerPos;
		$str .= $counter." & ".$row["articleName"]." & ".$productAmount." & ".$articlePriceStr.' & '.$pricePerPosStr.'\\\\'."\n";
		//$str .= "\hline\n";
	}
	
	$str .= "\hline\n";
	$str .= '\multicolumn{4}{r}{Summe netto} & '.number_format($sumPrice,2,',','.').'\\\\'."\n";	
	$str .= '\multicolumn{4}{r}{zzgl. 20 \% MwSt.} & '.number_format($sumPrice*0.2,2,',','.').'\\\\'."\n";	
	$str .= "\hline\n";
	$str .= '\multicolumn{4}{r}{Summe brutto} & '.number_format($sumPrice*1.2,2,',','.').'\\\\'."\n";	
	$str .= '\end{tabular}'."\n";
	$str .= '\end{center}'."\n";

	exec('mkdir -p tmp',$output,$returnvar);
	exec('mkdir -p downloads',$output,$returnvar);
	chmod("tmp", 0777);
	chmod("downloads", 0777);
	file_put_contents("tmp/output-content.tex", $str);

	$texStr = file_get_contents("Backbone.tex");

    $texStr = preg_replace(getPattern('renr'),'${1}'.$reNrHash.'${3}',$texStr);
    $texStr = preg_replace(getPattern('ordernr'),'${1}'.$orderNrHash.'${3}',$texStr);
	$texStr = preg_replace(getPattern('bestdat'),'${1}'.$orderDate.'${3}',$texStr);
	$texStr = preg_replace(getPattern('custname'),'${1}'.$custName.'${3}',$texStr);
	$texStr = preg_replace(getPattern('custstreet'),'${1}'.$custStreet.'${3}',$texStr);
	$texStr = preg_replace(getPattern('custtown'),'${1}'.$custTown.'${3}',$texStr);
	$texStr = preg_replace(getPattern('custcontact'),'${1}'.$contactName.'${3}',$texStr);

	file_put_contents("tmp/output.tex", $texStr);
	chmod("tmp/output-content.tex", 0777);
	chmod("tmp/output.tex", 0777);
	$filename = $custName.'-'.$reNrHash;
    exec('(latexmk -cd -f -interaction=batchmode -jobname="tmp/output-'.$filename.'" -pdf "/var/www/html/tmp/output.tex" && mv "/var/www/html/tmp/output-'.$filename.'.pdf" "/var/www/html/downloads/'.$filename.'.pdf" && latexmk -jobname="tmp/output-'.$filename.'" -C) > /dev/null 2>&1 &');
    

    function getPattern($str){
        return '/(\\\\newcommand\{\\\\'.$str.'\}\{)([^\}]*)(\})/';
    }
    
    function wp_normalize_path( $path ) {
        $path = str_replace( '\\', '/', $path );
        $path = preg_replace( '|(?<=.)/+|', '/', $path );
        if ( ':' === substr( $path, 1, 1 ) ) {
                $path = ucfirst( $path );
        }
        return $path;
	}

	$_SESSION['waitForDownload'][$filename] = 'notViewed';
	
	?>