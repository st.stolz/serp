<div class="row">
    <form class="col s12" method="post" action="<?=basename($_SERVER['REQUEST_URI'])?>">
        <input type="hidden" name="add_article" value="true"> 
        <input type="hidden" name="form_hash" value="<?=$form_hash?>"> 
        <div class="row">
            <div class="input-field col s4">
                <input id="article_name" name="article_name" type="text" class="validate">
                <label for="article_name">Product Name</label>
            </div>
            <div class="input-field col s4">
                <i class="material-icons prefix">euro_symbol</i>
                <input id="price" name="price" type="text" class="validate">
                <label for="price">Price</label>
            </div>
            <div class="input-field col s4">
                <button class="btn waves-effect waves-light" type="submit" name="action">Add new
                    <i class="material-icons right">send</i>
                </button>
            </div>
        </div>
      
    </form>
</div>